import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


URL = "https://weather.rambler.ru/v-kazani/today/"
HEADERS = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "user-agent": UserAgent().chrome,
}


def get_html(url, headers, params=None):
    r = requests.get(url=url, headers=headers, params=params)
    return r


def get_content(html:str):
    soup = BeautifulSoup(html, features="html.parser")
    cards = {}
    table = soup.find_all("div", class_="_27-x")
    present = soup.find_all("div", class_="_27-x")
    for item in range(1, 9):
        cards[item] = [table[item].text, present[item + 35].text]
    return cards


def main_weather():
    html = get_html(url=URL, headers=HEADERS)
    if html.status_code == 200:
        value = get_content(html=html.text)
    else:
        value = "Error (No connection)"
    return value


if __name__ == '__main__':
    result = main_weather()
    for i in result:
        print(result[i])
    print(result)