import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


URL = "https://www.cbr.ru/currency_base/daily/"
HEADERS = {
    "useragent": UserAgent().chrome,
    'accept': '*/*'
}


def parse_bitcoin():
    url = requests.get(URL, HEADERS)
    soup = BeautifulSoup(url.content, 'html.parser')
    values = soup.find_all('td')
    i = 1
    dict = {}

    while i < len(values):
        if values[i].get_text() == 'USD':
            dict['USD'] = float((values[i + 3].get_text()).replace(",", "."))
        elif values[i].get_text() == 'CNY':
            dict['CNY'] = float((values[i + 3].get_text()).replace(",", "."))
        elif values[i].get_text() == 'EUR':
            dict['EUR'] = float((values[i + 3].get_text()).replace(",", "."))
        i += 5

    return dict
