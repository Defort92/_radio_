import requests
import datetime
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


HOST_TAT_INFORM = "https://www.tatar-inform.ru"
URL_TAT_INFORM = "https://www.tatar-inform.ru"
URL_MAIN_NEWS_TAT_INFORM = "https://www.tatar-inform.ru/news/widget/list/temy"
MONTHS = {
    "1": "января",
    "2": "февраля",
    "3": "марта",
    "4": "апреля",
    "5": "мая",
    "6": "июня",
    "7": "июля",
    "8": "августа",
    "9": "сентября",
    "10": "октября",
    "11": "ноября",
    "12": "декабря"
}
HEADERS = {
    "useragent": UserAgent().chrome,
    'accept': '*/*'
}


def get_html(url, headers, params=None):
    req = requests.get(url=url, headers=headers, params=params)
    return req


def get_content_main_news(html: str, num_news: int = 5) -> list:
    global MONTHS
    # date_now = datetime.datetime.now().date()
    # month_now = str(datetime.datetime.now().month)
    soup = BeautifulSoup(html, features="html.parser")
    buckets = soup.find_all("div", class_="list-item__body")
    news = []
    i = 0

    for bucket in buckets:
        # pub_date = bucket.find("div", class_="list-item__date").get_text().replace(" ", "").split("\n")[2]
        # pub_date = pub_date[0:2] + "-" + month_now + "-" + pub_date[-4:]
        # pub_date = datetime.datetime.strptime(pub_date, '%d-%m-%Y').date()
        # if i >= num_news or (date_now - pub_date).days != 0:
        #     break

        news.append({
            "href": bucket.find("a", class_="list-item__title").get("href"),
            "title": bucket.find("a", class_="list-item__title").get_text(strip=True).replace(u"\xa0", " "),
            "description":
                bucket.find("div", class_="list-item__description").get_text(strip=True).replace(u"\xa0", " "),
            # "pub_date": pub_date
        })
        i += 1
    return news


def get_top_read_content(html: str):
    global HEADERS
    soup = BeautifulSoup(html, features="html.parser")
    buckets = soup.find_all("div", class_="news-top__item")
    news = []

    for bucket in buckets:
        url = bucket.find("a", class_="news-top__title").get("href")
        html = get_html(url=url, headers=HEADERS)
        if html.status_code//100 == 2:
            soup2 = BeautifulSoup(html.text, features="html.parser")
            main_content = soup2.find("div", class_="page-main__text").get_text().split("\n")[1]
            news.append({
                "href": url,
                "title": bucket.find("a", class_="news-top__title").get_text(strip=True),
                "description": main_content,
                "pub_date": datetime.datetime.now().date()
            })
        else:
            return None

    return news


def main_parse():
    html = get_html(url=URL_MAIN_NEWS_TAT_INFORM, headers=HEADERS)
    if html.status_code == 200:
        main_news = get_content_main_news(html=html.text, num_news=3)
    else:
        main_news = "Error (No connection)"

    html = get_html(url=URL_TAT_INFORM, headers=HEADERS)
    if html.status_code == 200:
        top_read_news = get_top_read_content(html=html.text)
    else:
        top_read_news = "Error (No connection)"

    return main_news, top_read_news
