import telebot
import datetime

from telebot import types
from main.parse_tat_inform import main_parse as parse_news
from main.parse_exchange_rates import parse_bitcoin
from main.parse_bitcoin_rates import main_for_bitcoin, second_main_for_bitcoin
from weather.pogoda import main_weather
from music_parser.music_loader import main_music
from speaker.from_text_to_speach import to_speach
from main.parse_tat_inform import MONTHS


# ПРИМЕРЫ для редактирования сообщений ботом (parse_mode="html")
# <b>bold</b>, <strong>bold</strong> - жирный
# <i>italic</i>, <em>italic</em> - наклон
# <u>underline</u>, <ins>underline</ins> - подчеркнутый
# <s>strikethrough</s>, <strike>strikethrough</strike>, <del>strikethrough</del> - зачеркнутый
# <b>bold <i>italic bold <s>italic bold strikethrough</s> <u>underline italic bold</u></i> bold</b> - просто пример
# <a href="http://www.example.com/">inline URL</a> - гипер ссылка
# <a href="tg://user?id=123456789">inline mention of a user</a> - упоминание пользователя через айдишник


# константы для кнопок
I_WANT_NEWS = "Новости"
I_WANT_RADIO = "Запустить радио"
I_WANT_WEATHER = "Погода"
I_WANT_EXCHANGE_RATES = "Курс валют"
AUDIO_FORMAT = "Аудио форматом"
TEXT_FORMAT = "Текстовым форматом"
# ...
TOKEN = "1848033684:AAE2HmYv9Kzx5aGrK7zvUJ-LkcCONRYRcv0"
# ссылки
URL_TAT_INFORM = "https://www.tatar-inform.ru/"
URL_WEATHER = "https://weather.rambler.ru/v-kazani/today/"
URL_RUS_BANK = "https://www.cbr.ru/"
URL_BITCOIN = "https://www.sravni.ru/valjuty/info/btc-rub-1/"
# чтобы не искать куда потом закидывать эту строку
HELP_MESSAGE = f"""🤖Что я умею: 

•  📰 Делиться свежими новостями.
Для того чтобы узнать последние новости Татарстана, предлагаемые сайтом <a href="{URL_TAT_INFORM}">Татинформ</a>, нажмите на интерактивной панели кнопку "{I_WANT_NEWS}". 
Вам будет предложено выбрать формат: если вы хотите получить аудиоверсию, то нажмите "{AUDIO_FORMAT}", а если вам хочется прочитать новости самим, то нажмите "{TEXT_FORMAT}".

•  🌦 Рассказывать о погоде на ближайшие четыре часа.
Чтобы выяснить, будет ли дождь в ближайшее время, нажмите "{I_WANT_WEATHER}". 
Вам будет представлена информация с сайта <a href="{URL_WEATHER}">Рамблер/погода</a> о температуре, влажности, осадках на следующие четыре часа. 
Вам также будет предложено выбрать формат: если вы хотите получить аудиоверсию прогноза, то нажмите "{AUDIO_FORMAT}", а если вам хочется посмотреть погоду самим, то нажмите "{TEXT_FORMAT}".

•  📉 Показывать актуальный курс валют.
Чтобы посмотреть курс валют, нажмите "{I_WANT_EXCHANGE_RATES}". Информация будет предоставлена с сайта <a href="{URL_RUS_BANK}">ЦБ РФ</a>. 
Курс доллара США, евро, китайских юаней и, кроме того, биткоина можно также услышать в двух форматах: для аудиоформата нажмите "{AUDIO_FORMAT}", для текстового – "{TEXT_FORMAT}". 

•  📻 Включать радио.
Для того чтобы начать работу радио, нажмите "{I_WANT_RADIO}". 
Если выбрать "{AUDIO_FORMAT}", то электронный диктор расскажет вам свежие новости, а также о погоде на ближайшее время и о курсе валют. 
После нажатия кнопки "{TEXT_FORMAT}" вы получите ту же информацию, но в виде сообщений."""


bot = telebot.TeleBot(TOKEN)
print(bot.get_me())
news_, radio_, weather_, exchange_rates_= 0, 0, 0, 0  # переменные отвечающие за то, что надо отправлять пользователю на данный момент


# =====================================================================================================================


def create_start_markup():
    """
        создает начальную клавиатуру
        :return: начальная клава
    """
    start_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton(I_WANT_NEWS)
    item4 = types.KeyboardButton(I_WANT_RADIO)
    item3 = types.KeyboardButton(I_WANT_WEATHER)
    item2 = types.KeyboardButton(I_WANT_EXCHANGE_RATES)
    start_markup.add(item1, item2, item3, item4)
    return start_markup


def create_audio_vs_text_markup():
    """
        создает клавиатуру для выбора формата передачи информации
        :return: клава для выбора формата вывода
    """
    choice_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton(AUDIO_FORMAT)
    item2 = types.KeyboardButton(TEXT_FORMAT)
    choice_markup.add(item1, item2)
    return choice_markup


def send_music(message):
    main_music()
    song = "../music_parser/song.mp3"

    audio = open(song, 'rb')
    bot.send_audio(message.chat.id, audio)
    audio.close()


def send_fresh_news(message):
    """
        отправка новостей (всех новостей)
        @params message - информация о сооющении от пользователя
    """
    main_news, top_read_news = parse_news()
    not_found_main_news = False
    not_found_top_news = False

    if main_news != "Error (No connection)":
        for news in main_news:
            title = news["title"]
            href = news["href"]
            description = news["description"]

            title = f"<b>{title}</b>"
            description = f"\n\n{description}\n\n"
            href = f"<a href=\"{href}\">Читать на официальном сайте</a>"
            final_text = title + description + href

            bot.send_message(message.chat.id, final_text, parse_mode="html")
    else:
        not_found_main_news = True

    if top_read_news != "Error (No connection)":
        for news in top_read_news:
            title = news["title"]
            href = news["href"]
            description = news["description"]

            title = f"<b>{title}</b>"
            description = f"\n\n{description}\n\n"
            href = f"<a href=\"{href}\">Читать на официальном сайте</a>"
            final_text = title + description + href

            bot.send_message(message.chat.id, final_text, parse_mode="html")
    else:
        not_found_top_news = True

    if not_found_top_news and not_found_main_news:
        error_text = f"Не удалось подключиться к сайту <a href=\"{URL_TAT_INFORM}\">TatInform</a>"
        bot.send_message(message.chat.id, error_text, parse_mode="html")


def send_weather(message):
    """
        отправка информации о погоде
        @params message - информация о сооющении от пользователя
    """
    weather_dict = main_weather()
    date_now = datetime.datetime.now().time()
    point_start = date_now.hour // 3
    final_text = ""

    if weather_dict == "Error (No connection)":
        error_text = f"Не удалось подключиться к сайту <a href=\"{URL_WEATHER}\">Рамблер\\погода</a>"
        bot.send_message(message.chat.id, final_text, parse_mode="html")
        return

    for i in weather_dict:
        if i < point_start:
            continue

        if i == point_start:
            final_text = f" 🌦 <b>ПОГОДА НА ДАННЫЙ МОМЕНТ:</b>\n   " \
                         f"<b>Температура  --  {weather_dict[i][0]}\n</b>   " \
                         f"<b>Вероятноясть осадков  --  {weather_dict[i][1]}</b>\n\n"
            continue

        final_text += f"<b>{int(i) * 3}:00</b> Температура  --  {weather_dict[i][0]}\n"
        final_text += f"           Вероятноясть осадков  --  {weather_dict[i][1]}\n\n"

    bot.send_message(message.chat.id, final_text, parse_mode="html")


def launch_radio_info_text(message):
    """
        запуск радио в текстовом формате
        @params message - информация о сооющении от пользователя
    """
    send_fresh_news(message=message)
    send_music(message=message)

    send_weather(message=message)
    send_music(message=message)

    send_exchange_rates(message=message)
    send_music(message=message)


def launch_radio_info_audio(message):
    """
        запуск радио в аудио формате
        @params message - информация о сооющении от пользователя
    """
    text_news_for_audio_interpretation(message=message)
    send_music(message=message)

    text_exchange_rates_for_audio_interpretation(message=message)
    send_music(message=message)

    text_weather_for_audio_interpretation(message=message)
    send_music(message=message)


def send_exchange_rates(message):
    """
        отправка курса валют
        @params message - информация о сооющении от пользователя
    """
    dict_ = parse_bitcoin()
    cny = round(dict_["CNY"], 2)
    eur = round(dict_["EUR"], 2)
    usd = round(dict_["USD"], 2)
    bit = second_main_for_bitcoin()
    date_now = datetime.datetime.now().date()
    final_string = f"Актуальный курс валют  от <a href=\"{URL_RUS_BANK}\">ЦБ РФ</a> "
    final_string += f"на {date_now.day}:{date_now.month}:{date_now.year}\n\n"
    final_string += f" Евро                         1 €  --  {eur} ₽\n"
    final_string += f" Доллар США         1 $  --  {usd} ₽\n"
    final_string += f" Китайский юань 1 ¥  --  {cny} ₽\n"
    if bit != "Error (No connection)":
        final_string += f" BITCOIN                  1 ₿  -- {bit} ₽"

    bot.send_message(message.chat.id, final_string, parse_mode="html")


def send_bitcoin_rates(message):
    """
        отправка курса валют
        @params message - информация о сооющении от пользователя
    """
    bitcoin = main_for_bitcoin()
    not_found_bitcoin = False

    if bitcoin != "Error (No connection)":
        final_text = "На сегодня " + bitcoin
        bot.send_message(message.chat.id, final_text, parse_mode="html")
    else:
        not_found_bitcoin = True

    if not_found_bitcoin:
        href = "https://www.sravni.ru/valjuty/info/btc-rub-1/"
        error_text = f"Не удалось подключиться к сайту <a href=\"{href}\">Sravni.ru</a>"
        bot.send_message(message.chat.id, error_text, parse_mode="html")


def text_news_for_audio_interpretation(message):
    """
        Функция отвечает за создание строки содержащей главные и читаемые
        @params message - информация о сооющении от пользователя
        @return  -  строку, содержащую новости
    """
    main_news, top_read_news = parse_news()

    final_text = ""

    if main_news != "Error (No connection)":
        for news in main_news:
            title = news["title"] + ". "
            description = news["description"] + ". "
            final_text += title + description

    if top_read_news != "Error (No connection)":
        for news in top_read_news:
            title = news["title"] + ". "
            description = news["description"] + ". "
            final_text += title + description

    to_speach(final_text)
    audio = open(r'../speaker/speach.mp3', 'rb')
    bot.send_audio(message.chat.id, audio)
    audio.close()


def text_exchange_rates_for_audio_interpretation(message):
    """
        функуия создает строку, содержащую урчы валют
        :return курсы валют
    """
    dict_ = parse_bitcoin()
    bit = second_main_for_bitcoin()
    cny = dict_["CNY"]
    eur = dict_["EUR"]
    usd = dict_["USD"]

    date_now = datetime.datetime.now().date()
    rates_string = f"Актуальный курс валют  от ЦБ РФ "
    if date_now.day == 3:
        rates_string += f"на {date_now.day}е:{MONTHS[str(date_now.month)]}й:{date_now.year}ого года\n\n"
    else:
        rates_string += f"на {date_now.day}ое:{MONTHS[str(date_now.month)]}й:{date_now.year}ого года\n\n"

    rates_string += f" Один евро  {int(eur)} рубля {int((float(eur%1)*100))} копеек."
    rates_string += f" Один доллар США  {int(usd)} рубля {int((float(usd%1)*100))} копеек."
    rates_string += f" Один Китайский юань  {int(cny)} рубля {int((float(cny%1)*100))} копеек."

    if bit != "Error (No connection)":
        rates_string += f"Один биткоин {str(int(bit))} рубля."

    to_speach(rates_string)
    audio = open(r'../speaker/speach.mp3', 'rb')
    bot.send_audio(message.chat.id, audio)
    audio.close()


def text_weather_for_audio_interpretation(message):
    """
        функуия создает строку, содержащую информацию о погоде
        :return строка с инфой о погоде
    """
    weather_dict = main_weather()
    date_now = datetime.datetime.now().time()
    point_start = date_now.hour // 3
    final_text = ""

    for i in weather_dict:
        if i < point_start:
            continue

        c = int(weather_dict[i][0].replace("°", ""))
        pos = float(weather_dict[i][1])

        if i == point_start:
            final_text = f"Температура  на данный момент составляет {c} "
            if c % 10 == 1:
                final_text += "градус. "
            elif c % 10 in [2, 3, 4]:
                final_text += "градуса. "
            else:
                final_text += "градусов. "

            final_text += f". Вероятность осадков {int(pos)} "
            if pos % 1 != 0:
                final_text += "процента. "
            elif 10 < pos < 20:
                final_text += "процентов."
            elif pos % 10 == 1:
                final_text += "процент. "
            elif pos % 10 in [2, 3, 4]:
                final_text += "процента. "
            else:
                final_text += "процентов. "
            continue

        final_text += f"В {int(i) * 3} ноль ноль, Температура составит {c} "
        if c % 10 == 1:
            final_text += "градус. "
        elif c % 10 in [2, 3, 4]:
            final_text += "градуса. "
        else:
            final_text += "градусов. "
        final_text += f"Вероятноясть осадков {int(pos)} "
        if pos % 1 != 0:
            final_text += "процента. "
        elif pos % 10 == 1:
            final_text += "процент. "
        elif pos % 10 in [2, 3, 4]:
            final_text += "процента. "
        else:
            final_text += "процентов. "

        if i == point_start + 3:
            break

    to_speach(final_text)
    audio = open(r'../speaker/speach.mp3', 'rb')
    bot.send_audio(message.chat.id, audio)
    audio.close()


def send_audio_information(message):
    """
        функция отвечает за отправку нужной информации аудио сообщениями
        @params message - информация о сообщении от пользователя
    """
    global news_, radio_, weather_, exchange_rates_
    if news_ == 1:
        text_news_for_audio_interpretation(message=message)
    elif radio_ == 1:
        launch_radio_info_audio(message=message)
    elif weather_ == 1:
        text_weather_for_audio_interpretation(message=message)
    elif exchange_rates_ == 1:
        text_exchange_rates_for_audio_interpretation(message=message)

    markup = create_start_markup()

    bot.send_message(message.chat.id, "Готово, жду новых команд)", reply_markup=markup)
    news_, radio_, weather_, exchange_rates_ = 0, 0, 0, 0


def send_text_information(message):
    """
        функция отвечает за отправку нужной информации текстом
        @params message - информация о сооющении от пользователя
    """
    global news_, radio_, weather_, exchange_rates_
    if news_ == 1:
        send_fresh_news(message=message)
    elif radio_ == 1:
        launch_radio_info_text(message=message)
    elif weather_ == 1:
        send_weather(message=message)
    elif exchange_rates_ == 1:
        send_exchange_rates(message=message)

    markup = create_start_markup()

    bot.send_message(message.chat.id, "Готово, джу новых команд)", reply_markup=markup)
    news_, radio_, weather_ = 0, 0, 0


def offer_a_choice(chat_id):
    global news_, radio_, weather_, exchange_rates_
    markup = create_audio_vs_text_markup()

    text_message = "Отлично, теперь Вам надо выбрать тип данных,"
    if news_ == 1:
        text_message += " которым Вы хотите получить информацию о новостях"
    elif radio_ == 1:
        text_message += " посредством которого я буду Вам вещать =)"
    elif weather_ == 1:
        text_message += " которым Вы хотите получить информацию о погоде"
    elif exchange_rates_ == 1:
        text_message += " которым Вы хотите получить курс валют"

    bot.send_message(chat_id, text_message, reply_markup=markup)


# =====================================================================================================================


@bot.message_handler(commands=['start'])
def welcome(message):
    # keyboard   (потом переработаю если будет не лень)
    markup = create_start_markup()

    time_now = datetime.datetime.now()
    some_words: str = ""
    message_for_help = "Чтобы подробнее узнать о моих возможностях, введите команду /help"

    if 1 <= time_now.hour < 8:
        some_words += f"Доброе утро, {message.from_user.first_name}, меня зовут {bot.get_me().first_name}."
    elif 8 <= time_now.hour < 17:
        some_words += f"Добрый день, {message.from_user.first_name}, меня зовут {bot.get_me().first_name}."
    else:
        some_words += f"Добрый вечер, {message.from_user.first_name}, меня зовут {bot.get_me().first_name}."

    bot.send_message(message.chat.id, some_words, parse_mode='html', reply_markup=markup)
    bot.send_message(message.chat.id, message_for_help, parse_mode='html', reply_markup=markup)


@bot.message_handler(commands=['help'])
def help_him(message):
    help_message_text = HELP_MESSAGE  # we need to fix it
    bot.send_message(message.chat.id, help_message_text, parse_mode='html')


@bot.message_handler(content_types=['text'])
def talk_to_him(message):
    global news_, radio_, weather_, exchange_rates_
    if message.text == I_WANT_NEWS:
        news_, radio_, weather_, exchange_rates_ = 1, 0, 0, 0
        offer_a_choice(message.chat.id)

    elif message.text == I_WANT_RADIO:
        news_, radio_, weather_, exchange_rates_ = 0, 1, 0, 0
        offer_a_choice(message.chat.id)

    elif message.text == I_WANT_WEATHER:
        news_, radio_, weather_, exchange_rates_ = 0, 0, 1, 0
        offer_a_choice(message.chat.id)

    elif message.text == I_WANT_EXCHANGE_RATES:
        news_, radio_, weather_, exchange_rates_ = 0, 0, 0, 1
        offer_a_choice(message.chat.id)

    elif message.text == AUDIO_FORMAT:
        send_audio_information(message)

    elif message.text == TEXT_FORMAT:
        send_text_information(message)

    elif message.text == "Ура победа!":
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAECXnRgt9EAATaKHGl0M53uJOu6FnKFteAAArEAA1kDAwABCStZ4iOeoZYfBA")

    else:
        some_text = "Я не знаю, что ответить("
        bot.send_message(message.chat.id, some_text)
        bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAECXnJgt9Cge2yrejsUJY-TGoYB4D9rhwAC8isAAojEQiOGuNRACSizIx8E')


bot.polling(none_stop=True, interval=0)
