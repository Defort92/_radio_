import requests
import re
import random
from bs4 import BeautifulSoup
REQUEST_STATUS = 200


def get_html():
    url = 'https://ruv.hotmo.org/'
    r = requests.get(url)
    soup_ing = str(BeautifulSoup(r.content, "html.parser"))
    soup_ing = soup_ing.encode()
    with open("../music_parser/site.html", "wb") as file:
        file.write(soup_ing)


def get_songs():
    html_file = ("../music_parser/site.html")
    html_file = open(html_file, encoding='UTF-8').read()
    soup = BeautifulSoup(html_file, "html.parser")  # name of our soup

    song_amount = 0
    links = []
    for link in soup.find_all('a'):
        if re.search("/get/music/*", str(link.get('href'))) and song_amount != 10:
            links.append(str(link.get('href')))
            song_amount += 1
    return links


def save_songs(links):
    req = requests.get(links[random.randint(0, 9)]+"?spa=false", stream=True)

    if req.status_code == REQUEST_STATUS:
        with open("../music_parser/song.mp3", "wb") as song:
            song.write(req.content)


def main_music():
    get_html()
    save_songs(get_songs())
